package com.kunal.homeshopping.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kunal.homeshopping.dto.CartDTO;
import com.kunal.homeshopping.dto.LoginDTO;
import com.kunal.homeshopping.entity.Cart;
import com.kunal.homeshopping.entity.Product;
import com.kunal.homeshopping.entity.User;
import com.kunal.homeshopping.entity.WishList;
import com.kunal.homeshopping.service.IUserService;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

	@Autowired
	private IUserService userService;

	@PostMapping("/register")
	public User register(@RequestBody User user) {
		return userService.register(user);
	}

	@PostMapping("/login")
	public User login(@RequestBody LoginDTO loginDTO) {
		return userService.login(loginDTO);
	}

	@GetMapping("/logout/{userId}")
	public String logout(@PathVariable Long userId) {
		return userService.logout(userId);
	}

	@GetMapping("/getProducts")
	public List<Product> getProducts() {
		return userService.getProducts();
	}

	@GetMapping("/getSortedProducts/{sorted}")
	public List<Product> getSortedProducts(@PathVariable String sorted) {
		return userService.getSortedProducts(sorted);
	}

	@GetMapping("/getCategoryProducts/{category}")
	public List<Product> getCategoryProducts(@PathVariable String category) {
		return userService.getCategoryProducts(category);
	}

	@PostMapping("/addToCart")
	public List<Cart> addToCart(@RequestBody CartDTO cartDTO) {
		return userService.addToCart(cartDTO);
	}

	@PostMapping("/addToWishlist")
	public List<WishList> addToWishlist(@RequestBody CartDTO cartDTO) {
		return userService.addToWishlist(cartDTO);
	}

	@GetMapping("/getCart/{userId}")
	public List<Cart> getCart(@PathVariable Long userId) {
		return userService.getCart(userId);
	}

	@GetMapping("/getWishList/{userId}")
	public List<WishList> getWishList(@PathVariable Long userId) {
		return userService.getWishList(userId);
	}

	@DeleteMapping("/removeFromCart")
	public String removeFromCart(@RequestBody CartDTO cartDTO) {
		return userService.removeFromCart(cartDTO);
	}

	@DeleteMapping("/removeFromWishList")
	public String removeFromWishList(@RequestBody CartDTO wishListDTO) {
		return userService.removeFromWishList(wishListDTO);
	}

}
