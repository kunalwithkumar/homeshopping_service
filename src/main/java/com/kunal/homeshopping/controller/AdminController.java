package com.kunal.homeshopping.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kunal.homeshopping.dto.LoginDTO;
import com.kunal.homeshopping.entity.Admin;
import com.kunal.homeshopping.entity.Product;
import com.kunal.homeshopping.entity.User;
import com.kunal.homeshopping.service.IAdminService;

@RestController
@RequestMapping("/admin")
@CrossOrigin
public class AdminController {

	@Autowired
	private IAdminService adminService;

	@PostMapping("/register")
	public String addAdmin(@RequestBody Admin admin) {
		return adminService.addAdmin(admin);
	}

	@PostMapping("/login")
	public Admin login(@RequestBody LoginDTO loginDTO) {
		return adminService.login(loginDTO);
	}

	@GetMapping("/logout/{adminId}")
	public String logout(@PathVariable Long adminId) {
		return adminService.logout(adminId);
	}

	@GetMapping("/getUsers")
	public List<User> getUsers() {
		return adminService.getUsers();
	}

	@DeleteMapping("/removeUser/{userId}")
	public String removeUser(@PathVariable Long userId) {
		return adminService.removeUser(userId);
	}

	@GetMapping("/getProducts")
	public List<Product> getProducts() {
		return adminService.getProducts();
	}

	@PostMapping("/addProduct")
	public Product addProduct(@RequestBody Product product) {
		return adminService.addProduct(product);
	}

	@PutMapping("/updateProduct")
	public Product updateProduct(@RequestBody Product product) {
		return adminService.updateProduct(product);
	}

	@DeleteMapping("/removeProduct/{productId}")
	public String removeProduct(@PathVariable Long productId) {
		return adminService.removeProduct(productId);
	}

	@PostMapping("/bulkUpload")
	public List<Product> bulkUpload() {
		return adminService.bulkUpload();
	}

	@GetMapping("/getReport")
	public List<Product> getReport() {
		return adminService.getReport();
	}

	@GetMapping("/getStock")
	public List<Product> getStock() {
		return adminService.getStock();
	}
}
