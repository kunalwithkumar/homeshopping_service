package com.kunal.homeshopping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeShoppingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeShoppingServiceApplication.class, args);
	}

}
