package com.kunal.homeshopping.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kunal.homeshopping.entity.Product;

@Repository
public interface IProductRepo extends JpaRepository<Product, Long> {

	public Product findByProductName(String productName);

	public List<Product> findByCategory(String category);

}
