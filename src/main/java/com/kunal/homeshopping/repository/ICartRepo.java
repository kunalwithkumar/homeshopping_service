package com.kunal.homeshopping.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kunal.homeshopping.entity.Cart;
import com.kunal.homeshopping.entity.User;

@Repository
public interface ICartRepo extends JpaRepository<Cart, Long> {

	public List<Cart> findByUser(User user);

	@Query("from Cart where user.id  = ?1 AND product.id  = ?2")
	public Cart findByUserProduct(Long userId, Long productId);

}
