package com.kunal.homeshopping.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kunal.homeshopping.entity.User;
import com.kunal.homeshopping.entity.WishList;

@Repository
public interface IWishListRepo extends JpaRepository<WishList, Long> {

	public List<WishList> findByUser(User user);

	@Query("from WishList where user.id = ?1 AND product.id = ?2")
	public WishList findByUserProduct(Long userId, Long productId);

}
