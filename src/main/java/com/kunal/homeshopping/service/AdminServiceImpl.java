package com.kunal.homeshopping.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kunal.homeshopping.dto.LoginDTO;
import com.kunal.homeshopping.entity.Admin;
import com.kunal.homeshopping.entity.Product;
import com.kunal.homeshopping.entity.User;
import com.kunal.homeshopping.repository.IAdminRepo;
import com.kunal.homeshopping.repository.IProductRepo;
import com.kunal.homeshopping.repository.IUserRepo;

@Service
public class AdminServiceImpl implements IAdminService {

	@Autowired
	private IAdminRepo adminRepo;

	@Autowired
	private IUserRepo userRepo;

	@Autowired
	private IProductRepo productRepo;

	@Override
	public String addAdmin(Admin admin) {
		Admin admin2 = adminRepo.findByEmail(admin.getEmail());

		if (Objects.isNull(admin2)) {
			adminRepo.save(admin);
			return "Admin Added";
		}
		return "Already There";
	}

	@Override
	public Admin login(LoginDTO adminDTO) {
		Admin admin = adminRepo.findByEmail(adminDTO.getEmail());
		if (Objects.isNull(admin)) {
			return admin;
			// throw exception
		} else {
			if (adminDTO.getPassword().equals(admin.getPassword())) {
				admin.setIsActive(true);
				adminRepo.save(admin);
				return admin;
			}
			return admin;
			// throw exception
		}
	}

	@Override
	public String logout(Long userId) {
		Admin admin = adminRepo.findById(userId).orElse(null);// throw exception
		admin.setIsActive(false);
		adminRepo.save(admin);
		return "Logged Out";
	}

	@Override
	public List<User> getUsers() {
		return userRepo.findAll();
	}

	@Override
	public String removeUser(Long UserId) {
		User user = userRepo.findById(UserId).orElse(null);// throw exception
		userRepo.delete(user);
		return "Deleted";
	}

	@Override
	public List<Product> getProducts() {
		return productRepo.findAll();
	}

	@Override
	public Product addProduct(Product product) {
		Product product2 = productRepo.findByProductName(product.getProductName());
		if (Objects.isNull(product2)) {
			productRepo.save(product);
			return product;
		}

		return product;// throw exception
	}

	@Override
	public Product updateProduct(Product product) {
		return productRepo.save(product);
	}

	@Override
	public String removeProduct(Long productId) {
		Product product = productRepo.findById(productId).orElse(null);// throw exception
		productRepo.delete(product);
		return "Product Removed";
	}

	@Override
	public List<Product> bulkUpload() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Product> getReport() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Product> getStock() {
		// TODO Auto-generated method stub
		return null;
	}

}
