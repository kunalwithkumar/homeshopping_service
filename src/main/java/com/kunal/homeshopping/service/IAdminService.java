package com.kunal.homeshopping.service;

import java.util.List;

import com.kunal.homeshopping.dto.LoginDTO;
import com.kunal.homeshopping.entity.Admin;
import com.kunal.homeshopping.entity.Product;
import com.kunal.homeshopping.entity.User;

public interface IAdminService {

	public String addAdmin(Admin admin);

	public Admin login(LoginDTO adminDTO);

	public String logout(Long userId);

	public List<User> getUsers();

	public String removeUser(Long userId);

	public List<Product> getProducts();

	public Product addProduct(Product product);

	public Product updateProduct(Product product);

	public String removeProduct(Long productId);

	public List<Product> bulkUpload();

	public List<Product> getReport();

	public List<Product> getStock();

}
