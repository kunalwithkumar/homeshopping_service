package com.kunal.homeshopping.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.kunal.homeshopping.dto.CartDTO;
import com.kunal.homeshopping.dto.LoginDTO;
import com.kunal.homeshopping.entity.Cart;
import com.kunal.homeshopping.entity.Product;
import com.kunal.homeshopping.entity.User;
import com.kunal.homeshopping.entity.WishList;
import com.kunal.homeshopping.repository.ICartRepo;
import com.kunal.homeshopping.repository.IProductRepo;
import com.kunal.homeshopping.repository.IUserRepo;
import com.kunal.homeshopping.repository.IWishListRepo;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private IUserRepo userRepo;

	@Autowired
	private IProductRepo productRepo;

	@Autowired
	private ICartRepo cartRepo;

	@Autowired
	private IWishListRepo wishListRepo;

	@Override
	public User register(User user) {
		User user2 = userRepo.findByEmail(user.getEmail());
		if (Objects.isNull(user2)) {
			userRepo.save(user);
			return user;
		}

		return user;// Throw exception
	}

	@Override
	public User login(LoginDTO loginDTO) {
		User user = userRepo.findByEmail(loginDTO.getEmail());
		if (Objects.isNull(user)) {
			return user;
			// throw exception
		} else {
			if (loginDTO.getPassword().equals(user.getPassword())) {
				user.setIsActive(true);
				userRepo.save(user);

				return user;

			}
			return user;// throw exception
		}
	}

	@Override
	public String logout(Long userId) {
		User user = userRepo.findById(userId).orElse(null);// throw exception
		user.setIsActive(false);
		userRepo.save(user);
		return "Logged Out";
	}

	@Override
	public List<Product> getProducts() {
		return productRepo.findAll();
	}

	@Override
	public List<Product> getSortedProducts(String sorted) {
		return productRepo.findAll(Sort.by(sorted));
	}

	@Override
	public List<Product> getCategoryProducts(String category) {
		return productRepo.findByCategory(category);
	}

	@Override
	public List<Cart> addToCart(CartDTO cartDTO) {

		User user = userRepo.findById(cartDTO.getUserId()).orElse(null);// throw exception
		Product product = productRepo.findById(cartDTO.getProductId()).orElse(null);// throw exception

		Cart cart = new Cart();
		cart.setUser(user);
		cart.setProduct(product);
		cart.setQuantity(cartDTO.getQuantity());
		cartRepo.save(cart);

		return cartRepo.findAll();
	}

	@Override
	public List<WishList> addToWishlist(CartDTO cartDTO) {
		User user = userRepo.findById(cartDTO.getUserId()).orElse(null);// throw exception
		Product product = productRepo.findById(cartDTO.getProductId()).orElse(null);// throw exception

		WishList wishList = new WishList();
		wishList.setProduct(product);
		wishList.setUser(user);
		wishList.setQuantity(cartDTO.getQuantity());
		wishListRepo.save(wishList);
		return wishListRepo.findAll();
	}

	@Override
	public List<Cart> getCart(Long userId) {

		User user = userRepo.findById(userId).orElse(null);// throw exception
		return cartRepo.findByUser(user);
	}

	@Override
	public List<WishList> getWishList(Long userId) {

		User user = userRepo.findById(userId).orElse(null);// throw exception
		return wishListRepo.findByUser(user);
	}

	@Override
	public String removeFromCart(CartDTO cartDTO) {

		Cart cart = cartRepo.findByUserProduct(cartDTO.getUserId(), cartDTO.getProductId());
		cartRepo.delete(cart);
		return "Removed From Cart";
	}

	@Override
	public String removeFromWishList(CartDTO wishListDTO) {

		WishList wishList = wishListRepo.findByUserProduct(wishListDTO.getUserId(), wishListDTO.getProductId());
		wishListRepo.delete(wishList);
		return "Removed From WishList";
	}

}
