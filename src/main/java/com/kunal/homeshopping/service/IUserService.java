package com.kunal.homeshopping.service;

import java.util.List;

import com.kunal.homeshopping.dto.CartDTO;
import com.kunal.homeshopping.dto.LoginDTO;
import com.kunal.homeshopping.entity.Cart;
import com.kunal.homeshopping.entity.Product;
import com.kunal.homeshopping.entity.User;
import com.kunal.homeshopping.entity.WishList;

public interface IUserService {

	public User register(User user);

	public User login(LoginDTO loginDTO);

	public String logout(Long userId);

	public List<Product> getProducts();

	public List<Product> getSortedProducts(String sorted);

	public List<Product> getCategoryProducts(String category);

	public List<Cart> addToCart(CartDTO cartDTO);

	public List<WishList> addToWishlist(CartDTO cartDTO);

	public List<Cart> getCart(Long userId);

	public List<WishList> getWishList(Long userId);

	public String removeFromCart(CartDTO cartDTO);

	public String removeFromWishList(CartDTO wishListDTO);

}
