package com.kunal.homeshopping.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartDTO {

	@NotNull
	private Long userId;
	@NotNull
	private Long productId;
	@NotNull
	private Long quantity;

}
